using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{
    // Move speed.
    public float speed;

    // Waypoints.
    public Transform[] waypoints;

    // Actual target.
    private Transform target;
    private int destPoint;

    // Sprite renderer.
    public SpriteRenderer spriteRenderer;
    

    // How much we take damage upon collision.
    public int damageOnCollision;


    // Start is called before the first frame update
    void Start()
    {
        destPoint = 0;
        target = waypoints[destPoint];
    }

    // Update is called once per frame
    void Update()
    {
        // The direction we are aiming at.
        Vector3 dir = target.position - transform.position;

        // Apply translation.
        transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);

        // If we are very close to our target, go to the next one.
        if (Vector3.Distance(transform.position, target.position) < 0.3f)
        {
            destPoint = (destPoint+1) % waypoints.Length;
            target = waypoints[destPoint];

            // Flip the sprite when changing waypoint.
            spriteRenderer.flipX = !spriteRenderer.flipX;
        }


    }

    // Triggered when the enemy is collided.
    private void OnCollisionEnter2D(Collision2D collider)
    {
        if (collider.transform.CompareTag("Player"))
        {
            // We get the PlayerHealth instance and apply damages.
            PlayerHealth playerHealth = collider.transform.GetComponent<PlayerHealth>();
            playerHealth.TakeDamage(damageOnCollision);
        }
    }

}
