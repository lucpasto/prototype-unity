using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Inventory : MonoBehaviour
{

    // Total number of coins.
    public int coinAmount;

    // Unique Inventory instance.
    public static Inventory instance;

    // Text showing the number of coins.
    public TMP_Text coinText;

    // Called before Start().
    private void Awake()
    {
        if (instance != null)
        {
            Debug.Log("More than one Inventory instance in the scene.");
            return;
        }

        // Set this instance as the unique default Inventory instance.
        instance = this;
    }

    // Add coins to the inventory.
    public void AddCoins(int count)
    {
        coinAmount += count;
        coinText.text = coinAmount.ToString();
    }

    // Start is called before the first frame update
    void Start()
    {
        // Refresh the coint amount.
        AddCoins(0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
