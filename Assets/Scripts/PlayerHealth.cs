using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{

    // Player maximum health.
    public int maxHealth;

    // Player current health.
    public int currentHealth;

    // Player's healthbar.
    public HealthBar healthBar;

    // Invincibility tag.
    public bool isInvincible;

    // Player sprite renderer.
    public SpriteRenderer spriteRenderer;

    // Invincibility delay.
    public float invincibilityDelay;

    // Invincibility duration.
    public float invincibilityDuration;

    // Number of hit points lost for debug purpose.
    public int debugDamageTaken;


    // Unique Inventory instance.
    public static PlayerHealth instance;

    // Called before Start().
    private void Awake()
    {
        if (instance != null)
        {
            Debug.Log("More than one PlayerHealth instance in the scene.");
            return;
        }

        // Set this instance as the unique default Inventory instance.
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
        isInvincible = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            TakeDamage(debugDamageTaken);
        }
    }

    // Heal the player.
    public void HealPlayer(int amount)
    {
        currentHealth += amount;

        // Cap at maximum health.
        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }

        healthBar.SetHealth(currentHealth);
    }

    // Inflict damages on the player.
    public void TakeDamage(int damage)
    {
        // Take only damages if we are not invincible.
        if (!isInvincible)
        {
            currentHealth -= damage;
            healthBar.SetHealth(currentHealth);

            // If the player dies.
            if (currentHealth <= 0)
            {
                KillPlayer();
            }
            else
            {
                isInvincible = true;
                StartCoroutine(InvincibilityFlash());
                StartCoroutine(HandleInvinciblityDelay());
            }

        }
    }

    // Kills the player.
    public void KillPlayer()
    {
        // We freeze the player velocity and animation.
        PlayerController.instance.rb.velocity = Vector3.zero;
        PlayerController.instance.rb.isKinematic = true;
        PlayerController.instance.playerCollider.enabled = false;

        // Trigger the death animation.
        PlayerController.instance.animator.SetTrigger("PlayerElimination");

        GameOverManager.instance.OnPlayerDeath();
    }

    // Called whenever the player is invincible.
    public IEnumerator InvincibilityFlash()
    {
        while (isInvincible)
        {
            // Transparent character.
            spriteRenderer.color = new Color(1f, 1f, 1f, 0f);

            // Wait a bit.
            yield return new WaitForSeconds(invincibilityDelay);
            
            // Visible character.
            spriteRenderer.color = new Color(1f, 1f, 1f, 1f);

            // Wait a bit.
            yield return new WaitForSeconds(invincibilityDelay);
        }
    }

    // Called when the player takes damages.
    public IEnumerator HandleInvinciblityDelay()
    {
        // Wait a bit then disable invincibility.
        yield return new WaitForSeconds(invincibilityDuration);
        isInvincible = false;
    }
}
