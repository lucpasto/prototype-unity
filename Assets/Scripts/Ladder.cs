using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Ladder : MonoBehaviour
{
    // True when the player is in range of the ladder.
    public bool isInRange = false;

    // Player controller instance.
    private PlayerController playerController;

    // Floor collider.
    public BoxCollider2D floorCollider;

    // Text to show whenever we are in range.
    private TMP_Text interactText;

    // Called before Start().
    void Awake()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        interactText = GameObject.FindGameObjectWithTag("InteractUI").GetComponent<TMP_Text>();
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // If we climbing and pressing the climbing key, stop climbing.
        if (isInRange && playerController.isClimbing && Input.GetKeyDown(KeyCode.E))
        {
            playerController.isClimbing = false;
            floorCollider.isTrigger = false;
            return;
        }

        // If we are in range and pressing the climbing key, start climbing.
        if (isInRange && Input.GetKeyDown(KeyCode.E))
        {
            playerController.isClimbing = true;

            // We can pass through the floor collider.
            floorCollider.isTrigger = true;
        }
    }

    // Called upon collision.
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("Player"))
        {
            isInRange = true;
            interactText.enabled = true;
        }
    }

    // Called upon exiting the box collider.
    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.CompareTag("Player"))
        {
            isInRange = false;
            interactText.enabled = false;
            playerController.isClimbing = false;

            // Re-enable the floor collider.
            floorCollider.isTrigger = false;
        }
    }
}
