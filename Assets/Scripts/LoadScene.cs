using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    // Fade system animator.
    public Animator fadeSystem;

    // Player controller.
    private PlayerController playerController;

    // Called before Start().
    void Awake()
    {
        fadeSystem = GameObject.FindGameObjectWithTag("FadeSystem").GetComponent<Animator>();
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadNextScene(Collider2D collider, string sceneNameToLoad)
    {
        StartCoroutine(LoadNextSceneCoroutine(collider, sceneNameToLoad));
    }




    public IEnumerator LoadNextSceneCoroutine(Collider2D collider, string sceneNameToLoad)
    {
        // We freeze the player velocity.
        playerController.SetAnimatorStatus(false);
        collider.attachedRigidbody.velocity = Vector3.zero;
        collider.attachedRigidbody.isKinematic = true;

        fadeSystem.SetTrigger("FadeIn");
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(sceneNameToLoad);

        // We wait a bit and unfreeze the player.
        yield return new WaitForSeconds(1f);
        playerController.SetAnimatorStatus(true);
        collider.attachedRigidbody.isKinematic = false;
    }
}
