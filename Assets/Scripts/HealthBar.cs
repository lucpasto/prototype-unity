using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    // Healthbar slider.
    public Slider slider;

    // Color transition management (without shaders).
    public Gradient gradient;
    public Image fill;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Reset the max health.
    public void SetMaxHealth(int health)
    {
        slider.maxValue = health;
        SetHealth(health);

        // Change the fill color based on the gradient.
        fill.color = gradient.Evaluate(1f);

    }

    // Set the desired health.
    public void SetHealth(int health)
    {
        slider.value = health;

        // Change the fill color based on the gradient.
        fill.color = gradient.Evaluate(slider.normalizedValue);
    }
}
