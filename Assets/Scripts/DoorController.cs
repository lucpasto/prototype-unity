using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    // Scene name to load.
    public string sceneNameToLoad;

    // Scene loader.
    private LoadScene sceneLoader;

    // Called before Start().
    void Awake()
    {
        sceneLoader = GameObject.FindGameObjectWithTag("GameManager").GetComponent<LoadScene>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Called upon collision.
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("Player"))
        {
            sceneLoader.LoadNextScene(collider, sceneNameToLoad);
        }
    }
}
