using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    // Move speed.
    public float moveSpeed;

    // Climb speed.
    public float climbSpeed;

    // Jump force.
    public float jumpForce;

    // Player Rigidbody.
    public Rigidbody2D rb;

    // Default horizontal velocity.
    private Vector3 velocity = Vector3.zero;

    // Jump / grounded flags.
    private bool isJumping = false;
    private bool isGrounded;

    // Climbing flag.
    public bool isClimbing = false;


    // Grounded checkers.
    public Transform groundCheck;

    // Groundcheck radius (at the bottom of the player).
    public float groundCheckRadius;

    // Layer with which we want to check the collision.
    public LayerMask collisionLayer;

    // Animator.
    public Animator animator;

    // Sprite renderer.
    public SpriteRenderer spriteRenderer;

    // Horizontal movement float speed.
    private float horizontalMovement;

    // Vertical movement float speed.
    private float verticalMovement;

    // Player animator.
    public Animator playerAnimator;
    
    // Unique Inventory instance.
    public static PlayerController instance;

    // Player collider.
    public BoxCollider2D playerCollider;

    // Called before Start().
    private void Awake()
    {
        if (instance != null)
        {
            Debug.Log("More than one PlayerController instance in the scene.");
            return;
        }

        // Set this instance as the unique default Inventory instance.
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // If we press Jump and are grounded, we'll jump.
        if (Input.GetButtonDown("Jump") && isGrounded && !isClimbing)
        {
            isJumping = true;
        }


        // Flip the sprite along the X axis if needed.
        Flip(rb.velocity.x);


    }

    void FixedUpdate()
    {
        // Horizontal move speed.
        horizontalMovement = Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime;

        // Vertical move speed.
        verticalMovement = Input.GetAxis("Vertical") * climbSpeed * Time.deltaTime;


        float characterVelocity = Mathf.Abs(rb.velocity.x);
        animator.SetFloat("Speed", characterVelocity);
        animator.SetBool("isClimbing", isClimbing);

        // We check if we isGrounded checkers are overlapping with something else.
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, collisionLayer);

        // Then we move the player if the rigidbody is not frozen.
        if (!rb.isKinematic)
        {
            MovePlayer(horizontalMovement, verticalMovement);
        }
    }

    void MovePlayer(float horizontalMovement, float verticalMovement)
    {
        // If we are not climbing.
        if (!isClimbing)
        {
            // Horizontal velocity computation.
            Vector2 targetVelocity = new Vector2(horizontalMovement, rb.velocity.y);

            // Setting Rigidbody velocity with a smooth damp.
            if (rb.velocity != targetVelocity)
            {
                rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVelocity, ref velocity, .05f);
            }

            // If we jump, we'll add vertical force.
            if (isJumping)
            {
                rb.AddForce(new Vector2(0f, jumpForce));
                isJumping = false;
            }
        }
        // Else, we can move vertically.
        else
        {
            // Vertical velocity computation.
            Vector2 targetVelocity = new Vector2(0, verticalMovement);

            // Setting Rigidbody velocity with a smooth damp.
            rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVelocity, ref velocity, .05f);
        }
    }

    void Flip(float velocity)
    {
            // If we go forward, there is no flip.
            if (velocity > 0.1f)
            {
                spriteRenderer.flipX = false;
            }
            // If we go backward, there is a flip.
            else if (velocity < -0.1f)
            {
                spriteRenderer.flipX = true;
            }
    }


    // Called to draw Gizmos.
    private void OnDrawGizmos()
    {
        // Drawing the groundcheck Gizmo.
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(groundCheck.position, groundCheckRadius);
    }

    // Set the animator enable status.
    public void SetAnimatorStatus(bool b)
    {
        playerAnimator.enabled = b;
    }


}
