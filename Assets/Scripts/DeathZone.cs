using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathZone : MonoBehaviour
{
    // Player spawn point transform.
    private Transform playerSpawnTransform;

    // Fade system animator.
    private Animator fadeSystem;

    // Player controller.
    private PlayerController playerController;

    // Called before Start().
    void Awake()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        playerSpawnTransform = GameObject.FindGameObjectWithTag("PlayerSpawn").transform;
        fadeSystem = GameObject.FindGameObjectWithTag("FadeSystem").GetComponent<Animator>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Called upon collision.
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("Player"))
        {
            StartCoroutine(ReplacePlayer(collider));
        }
    }

    // Coroutine called for the fade.
    private IEnumerator ReplacePlayer(Collider2D collider)
    {
        // We freeze the player velocity and animation.
        playerController.SetAnimatorStatus(false);
        collider.attachedRigidbody.velocity = Vector3.zero;
        collider.attachedRigidbody.isKinematic = true;

        // Fade in and wait 1 second, then replace.
        fadeSystem.SetTrigger("FadeIn");
        yield return new WaitForSeconds(1f);
        collider.transform.position = playerSpawnTransform.position;

        // We wait a bit and unfreeze the player.
        yield return new WaitForSeconds(1f);
        playerController.SetAnimatorStatus(true);
        collider.attachedRigidbody.isKinematic = false;
    }
}
