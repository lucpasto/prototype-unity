using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour
{
    // Game over menu.
    public GameObject gameOverMenu;



    // Unique Inventory instance.
    public static GameOverManager instance;

    // Called before Start().
    private void Awake()
    {
        if (instance != null)
        {
            Debug.Log("More than one GameOverManager instance in the scene.");
            return;
        }

        // Set this instance as the unique default Inventory instance.
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Active the menu upon player death.
    public void OnPlayerDeath()
    {
        gameOverMenu.SetActive(true);
    }

    // Called when "RETRY" is clicked.
    public void RetryButton()
    {
        // Reload the current scene.
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        // Disabled the game over menu.
        gameOverMenu.SetActive(false);
    }

    // Called when "MAIN MENU" is clicked.
    public void MainMenuButton()
    {

    }

    // Called when "EXIT GAME" is clicked.
    public void ExitButton()
    {
        Application.Quit();
    }
}
