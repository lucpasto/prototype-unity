using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyOnLoadScene : MonoBehaviour
{
    // Objects to keep between scenes.
    public GameObject[] objectsToKeep;

    // Called before Start().
    void Awake()
    {
        
        // We keep every object in objectsToKeep.
        foreach (GameObject obj in objectsToKeep)
        {
            DontDestroyOnLoad(obj);
        }
        
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
